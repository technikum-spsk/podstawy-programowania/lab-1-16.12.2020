#include <iostream>
#include <fstream>

using namespace std;

int main() {
    fstream file;

    file.open("C:\\Programs\\text.txt", ios::in);
    if (!file.good()) {
        cout << "Plik nie istnieje" << endl;
    } else {
        while (!file.eof()) {
            string line;
            getline(file, line);
            cout << line << endl;
        }
    }
    file.close();
    return 0;
}
